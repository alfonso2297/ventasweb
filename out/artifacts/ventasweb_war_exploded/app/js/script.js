var session = false;

console.log("Ejecutandose el script principal");

//document.cookie = "username=John Doe";


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}

//setCookie('usuario','Alfonso',1);
//console.log(getCookie('usuario'));


//Función para cargar datos de la cookie
function loadUser(){
    console.log(getCookie('id'));
    console.log('en load user');
    if (getCookie('id') == '' && getCookie("nick") == "" && getCookie("nombres") == "" && getCookie("apellidos") == "" ) {
        console.log("Ninguna sesión previa iniciada");
        session = false;
    } else {
        
        user.id = getCookie('id');
        user.nick = getCookie('nick');
        user.nombres = getCookie("nombres");
        user.apellidos = getCookie("apellidos");
        console.log('Sesión restaurada');
        session = true;
        document.getElementById("saludo").innerHTML = "Bienvenido(a) "+ user.nombres + " " + user.apellidos;
    }
}


var user = {

    id : null,
    nick : null,
    nombres : null,
    apellidos : null

};

loadUser();





